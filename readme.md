# Laravel/Lumen Docker Scaffold

### **Description**

This will create a dockerized stack for a Laravel/Lumen application, consisted of the following containers:

-  **app**, your PHP application container

        PHP7.2 PHP7.2-fpm, Composer
    
-  **mysql**, MySQL database container ([mysql](https://hub.docker.com/_/mysql/) official Docker image)

#### **Directory Structure**
```
+-- src <project root>
+-- resources
|   +-- default
|   +-- nginx.conf
|   +-- supervisord.conf
|   +-- www.conf
+-- .gitignore
+-- Dockerfile
+-- docker-compose.yml
+-- readme.md <this file>
```

### **Setup instructions**

**Prerequisites:** 

* Depending on your OS, the appropriate version of Docker Community Edition has to be installed on your machine.  ([Download Docker Community Edition](https://hub.docker.com/search/?type=edition&offering=community))

**Installation steps:** 

1. Create a new directory in which your OS user has full read/write access and clone this repository inside.

2. Clone the API project into `src` folder.

3. Change the docker-compose.yml configurations according to your needs.

4. Open a new terminal/CMD, navigate to this repository root (where `docker-compose.yml` exists) and execute the following command:

    ```
    $ docker-compose up -d
    ```

    This will download/build all the required images and start the stack containers. It usually takes a bit of time, so grab a cup of coffee.

5. After the whole stack is up, enter the app container and follow the instructions from: `https://bitbucket.org/opensev/conexa-print-api/src/master/readme.md`

6. That's it! Navigate to [http://localhost](http://localhost) to access the application.

**Default configuration values** 

The following values should be replaced in your `.env` file if you're willing to keep them as defaults:
    
    DB_HOST=mysql_api
    DB_PORT=3306
    DB_DATABASE=conexa_print
    DB_USERNAME=root
    DB_PASSWORD=root
    
